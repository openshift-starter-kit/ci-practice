# OpenShift Pipelines(Tekton)によるCIハンズオン
本演習では OpenShift Pipelines(Tekton)を使用した継続的インテグレーション(Continuous Integration)の動作、及び、実装方法を理解することを目的として、以下の図に示すCIパイプライン、及び、Event Listenerを作成し、その動作確認を行います。

![picture 14](./handson/img/images/8a62fbc903a1026d198bffaf780d985a2975d9307726f6e051d307f7a10638d9.png)  

---
## Table of Contents
- [OpenShift Pipelines(Tekton)によるCIハンズオン](#openshift-pipelinestektonによるciハンズオン)
  - [Table of Contents](#table-of-contents)
  - [1. ハンズオン環境準備](#1-ハンズオン環境準備)
    - [1.1. OpenShift環境へのアクセス](#11-openshift環境へのアクセス)
    - [1.2. Gitリポジトリのクローン](#12-gitリポジトリのクローン)
  - [2. CIパイプラインの作成](#2-ciパイプラインの作成)
    - [2.1. プロジェクトの作成](#21-プロジェクトの作成)
    - [2.2. `Task` の作成](#22-task-の作成)
      - [2.2.1 ソースコード取得(git-clone)Taskの作成](#221-ソースコード取得git-clonetaskの作成)
      - [2.2.2 静的診断(sonarqube-scanner)Taskの作成](#222-静的診断sonarqube-scannertaskの作成)
      - [2.2.3 コンテナビルドTask](#223-コンテナビルドtask)
      - [2.2.4 イメージ脆弱性診断(trivy)Taskの作成](#224-イメージ脆弱性診断trivytaskの作成)
    - [2.3. `Pipeline`の作成](#23-pipelineの作成)
      - [2.3.1 Task設定](#231-task設定)
      - [2.3.2 パラメータ設定](#232-パラメータ設定)
      - [2.3.3 Pipeline作成](#233-pipeline作成)
    - [2.4 `PipelineRun`の定義作成](#24-pipelinerunの定義作成)
      - [2.4.1 Pipeline参照の定義](#241-pipeline参照の定義)
      - [2.4.2 パラメータ値の設定](#242-パラメータ値の設定)
    - [2.5 環境設定と動作確認](#25-環境設定と動作確認)
      - [2.5.1 git-clone用のSecret作成](#251-git-clone用のsecret作成)
      - [2.5.2 Workspace用PersistentVolumeClaimの作成](#252-workspace用persistentvolumeclaimの作成)
      - [2.5.3 CIパイプライン動作確認](#253-ciパイプライン動作確認)
  - [3. Event Listenerの作成](#3-event-listenerの作成)
    - [3.1 `TriggerBinding`の作成](#31-triggerbindingの作成)
    - [3.2 `TriggerTemplate`の作成](#32-triggertemplateの作成)
    - [3.3 `EventListener`の作成](#33-eventlistenerの作成)
      - [3.3.1 `TriggerBinding`と`TriggerTemplate`の参照設定](#331-triggerbindingとtriggertemplateの参照設定)
      - [3.3.2 `Interceptor`の設定](#332-interceptorの設定)
      - [3.3.3 `EventListener`の作成](#333-eventlistenerの作成)
    - [3.4 Route作成](#34-route作成)
    - [3.5 動作確認](#35-動作確認)
  - [4. GitリポジトリへのソースコードpushからCIパイプライン実行までの動作確認](#4-gitリポジトリへのソースコードpushからciパイプライン実行までの動作確認)
    - [4.1. GitリポジトリへのWebhook設定](#41-gitリポジトリへのwebhook設定)
    - [4.2. 実際の開発プロセスを想定したCIパイプライン動作確認](#42-実際の開発プロセスを想定したciパイプライン動作確認)

---

## 1. ハンズオン環境準備

### 1.1. OpenShift環境へのアクセス
最初に、使用するOpenShift環境へのアクセスを確認します。インストラクターから提示されたOpenShiftクラスタのURLをブラウザ開き、自身のユーザーアカウントでログイン可能か、確認して下さい。(ログイン画面で Identity Provider の選択肢が複数表示される場合は`htpasswd_provider`を選択してください。)

ログイン後、以下の手順で、Web Terminalを起動してください。

![webterminal](./handson/img/web-terminal.png)

### 1.2. Gitリポジトリのクローン
以下コマンドにより、環境変数USERの設定、CIパイプライン作成に使用するマニフェスト、及び、アプリケーションを格納するGitリポジトリをハンズオン実行環境にクローンします。

[注意] ネットワーク障害等により、Web Terminalへ再接続する必要がある場合、再接続後に環境変数の設定が消去されていないかご確認下さい。

```
# 環境変数の設定
cat << EOF >> ~/.bashrc
export USER=\$(oc whoami)
EOF
. ~/.bashrc

# マニフェストリポジトリのクローン
git clone http://$(oc -n ${USER}-develop get route gitea -o jsonpath='{.spec.host}')/gitea/ci-practice

# アプリケーションリポジトリのクローン
git clone http://$(oc -n ${USER}-develop get route gitea -o jsonpath='{.spec.host}')/gitea/cicd-sample-app
```

以下、上記コマンドにて使用したocコマンドのオプション説明となります。
- -n <プロジェクト名>
  - 指定したプロジェクト名の中に存在するリソースに対して、ocコマンド実行するよう指定
- get route <Routeリソース名>
  - 指定したRouteリソースの情報を取得
- -o jsonpath='{.spec.host}'
  - jsonpath(例: .spec.host)により、取得したリソース情報内から抽出したい情報を指定

以上をもちまして、ハンズオン環境準備は、完了となります。

## 2. CIパイプラインの作成
本章では、以下図の赤枠で囲まれたCIパイプラインの作成、及び、動作確認を行います。具体的には、以下図の右側に示されるTaskとPipelineリソース作成によるCIパイプラインの定義、及び、PipelineRunリソース作成によるCIパイプライン実行を通しての動作確認をyamlファイル編集により行います。

![picture 15](./handson/img/images/867195dddcd89073e0fbce1fb68ceeb6618cc0fd8b07ca71b2274ddd65c9d511.png)  

### 2.1. プロジェクトの作成
最初に、下図赤枠で示される、作成するリソースを格納するためのOpenShiftプロジェクトを作成します。

![picture 3](./handson/img/images/8459382ef69cacf5ed3bd59afb883bd81ed3abf15b9e7fae912e30dc50a5d182.png)  

OpenShiftプロジェクトは、 以下コマンドにより作成します。

```
oc new-project ci-${USER}
```

以下、上記コマンドにて使用したocコマンドのオプション説明となります。
- new-project <プロジェクト名>
  - 指定されたプロジェクト名で、OpenShiftプロジェクトを新規作成

### 2.2. `Task` の作成
次に、下図赤枠で示される、CIパイプライン処理の構成要素である`Task`リソースを作成します。

![picture 5](./handson/img/images/846005b3b09862d102ba9c44c0b9edb7b2eb3d66fde95980732d93f8cc03fb9c.png)  

#### 2.2.1 ソースコード取得(git-clone)Taskの作成
ソースコード取得のTaskは、git clone処理を記述したyamlファイル(~/ci-practice/handson/tasks/git-clone.yaml)を使用し、そのyamlファイルをOpenShiftへ適用することで、作成します。

最初に、Taskの定義方法に関しての理解を深める為に、~/ci-practice/handson/tasks/git-clone.yamlの内容をWeb Terminal上でcatコマンドを使用して、確認して下さい。特に、Task名や、どのようなコンテナイメージを使用しているかを確認し、どのような処理が定義されているか、確認して下さい。

```
cat ~/ci-practice/handson/tasks/git-clone.yaml
```

次に、ソースコード取得Taskを作成します。以下コマンドを実行して下さい。


```
oc apply -f ~/ci-practice/handson/tasks/git-clone.yaml -n ci-${USER}
```

以下、上記コマンドにて使用したocコマンドのオプション説明となります。
- oc apply -f <yamlファイル名>
  - 指定されたyamlファイルをOpenShiftへ入力し、リソースとして登録します。

次に、以下コマンドにより、ソースコード取得Taskが作成されていることを確認します。

```
oc get task -n ci-${USER}
```

出力例
```
NAME                AGE
git-clone           9s
```

以下、上記コマンドにて使用したocコマンドの説明となります。
- oc get <リソース定義名>- 指定されたリソース定義(例: Task, Pipeline等)から生成されたリソースを取得表示

以上をもちまして、ソースコード取得Task作成は、完了となります。

#### 2.2.2 静的診断(sonarqube-scanner)Taskの作成
静的診断のTaskは、[SonarQube](https://www.sonarqube.org/)を使用する記述を定義したyamlファイル(~/ci-practice/handson/tasks/sonarqube.yaml)を使用し、そのyamlファイルをOpenShiftへ適用することで、作成します。

最初に、Taskの定義方法に関しての理解を深める為に、~/ci-practice/handson/tasks/sonarqube.yamlの内容をWeb Terminal上でcatコマンドを使用して、確認して下さい。特に、Task名や、どのようなコンテナイメージを使用しているかを確認し、どのような処理が定義されているか、確認して下さい。

```
cat ~/ci-practice/handson/tasks/sonarqube.yaml
```

次に、静的診断Taskを作成します。以下コマンドを実行して下さい。

```
oc apply -f ~/ci-practice/handson/tasks/sonarqube.yaml -n ci-${USER}
```

次に、以下コマンドにより、静的診断Task(sonarqube-scanner)が作成されていることを確認します。

```
oc get task -n ci-${USER}
```

出力例
```
NAME                AGE
git-clone           9s
sonarqube-scanner   2s
```

以上をもちまして、静的診断Task作成は、完了となります。

#### 2.2.3 コンテナビルドTask

コンテナビルド用のTaskとしては、`ClusterTask`と呼ばれる、OpenShift Pipelinesインストール時に事前登録されるTaskであるs2i-nodejsを使用します。このため、ここでのTask作成作業は、不要となります。

ご参考までに、以下コマンドにより、利用可能なClusterTaskを確認することが可能となります。

```
oc get clustertask
```

出力例
```
---
NAME                       AGE
buildah                    17m
buildah-1-6-0              17m
git-cli                    17m
git-clone                  17m
<以下略>
```

#### 2.2.4 イメージ脆弱性診断(trivy)Taskの作成
イメージ脆弱性診断のTaskは、[Trivy](https://aquasecurity.github.io/trivy/dev/)を使用する記述を定義したyamlファイル(~/ci-practice/handson/tasks/trivy.yaml)を使用し、そのyamlファイルをOpenShiftへ適用することで、作成します。

最初に、Taskの定義方法に関しての理解を深める為に、~/ci-practice/handson/tasks/trivy.yamlの内容をWeb Terminal上でcatコマンドを使用して、確認して下さい。特に、Task名や、どのようなコンテナイメージを使用しているかを確認し、どのような処理が定義されているか、確認して下さい。


```
cat ~/ci-practice/handson/tasks/trivy.yaml
```

次に、イメージ脆弱性診断Taskを作成します。以下コマンドを実行して下さい。

```
oc apply -f ~/ci-practice/handson/tasks/trivy.yaml -n ci-${USER}
```

次に、以下コマンドにより、イメージ脆弱性診断Task(trivy)が作成されていることを確認します。

```
oc get task -n ci-${USER}
```

出力例
```
NAME                AGE
git-clone           7m25s
sonarqube-scanner   72s
trivy               8s
```

以上をもちまして、イメージ脆弱性診断Task作成は、完了となります。

### 2.3. `Pipeline`の作成
本節では、下図赤枠で示される`Pipeline`リソースを作成することで、CIパイプラインの定義を行います。具体的には、前節で作成したTask、及びClusterTaskを`Pipeline`内で参照する形で列挙することで、`Pipeline`を作成します。

![picture 9](./handson/img/images/dca000090302c37f993023c6f25f66f819e681266b4987ceba3a168b92dc291e.png)  



#### 2.3.1 Task設定
最初に、CIパイプラインを構築する方法を理解する目的で、前節で作成したTaskを使用して、Pipelineリソースを作成します。具体的には、ソースコード取得(git-clone)Taskに着目し、このソースコード取得TaskをPipelineリソースから参照する設定記述を~/ci-practice/handson/pipelines/handson-pipeline.yamlに対して行います(他のTaskに関しては、設定記述済みです)。

最初に、~/ci-practice/handson/pipelines/handson-pipeline.yamlをviエディタで開き、22行目の`tasks`から始まる、CIパイプラインで使用するタスクが、`- name`単位で複数列挙されていることを確認します。

```
  tasks:
  - name: git-clone-health
    taskRef:
      name: <REPLACE-WITH-TASK-NAME>
    params:
    - name: <REPLACE-WITH-PARAM-NAME>
      value: $(params.git-url)
    - name: revision
      value: $(params.git-revision)
    workspaces:
    - name: output
      workspace: shared-workspace
      
  - name: scan-app
    taskRef:
      name: sonarqube-scanner
    runAfter:
    - git-clone-health
    <以下略>
```

本演習で着目するソースコード取得(git-clone)TaskをCIパイプラインに組み込むために、`- name git-clone-health`配下の`taskRef`を設定します。`taskRef`配下の`name: <REPLACE-WITH-TASK-NAME>`の`<REPLACE-WITH-TASK-NAME>`を前節で作成したソースコード取得(git-clone)Taskの名前に設定して下さい。

**ヒント**
- Taskの名前は、以下で示す、~/ci-practice/handson/tasks/git-clone.yamlの`metadata.name`で定義された値(=git-clone)となります。
```
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: git-clone
  labels:
    app.kubernetes.io/version: "0.3"
```
- 以降の演習にて、yamlファイルに設定すべき内容が分からない/自信が無い場合は、~/ci-practice/handson/answers配下に正解を記述したファイルがありますので、こちらを適宜ご参照下さい。**正解用のコマンド**が正解を記述したファイルを実行するためのコマンドです。


#### 2.3.2 パラメータ設定
次に、CIパイプライン構築時に有用であるパラメータ設定方法を理解する目的で、前節で作成したTaskを使用して、Pipelineリソースからパラメータを使用する設定を行います。具体的には、ソースコード取得(git-clone)Taskに着目し、このソースコード取得(git-clone)Taskで宣言されているパラメータ(=url)に対して、Pipelineリソース上から値を設定する記述を行います。

最初に、ソースコード取得(git-clone)Taskのファイル~/ci-practice/handson/tasks/git-clone.yamlを開き、urlという名前のパラメータが宣言されている箇所を確認します。具体的には、以下の様な`params`で始まる記述箇所を確認します。

```
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: git-clone
...<省略>
  params:
    - name: url
      description: git url to clone
      type: string
```

次に、Pipelineリソース上にて、呼び出すTaskへのパラメータ設定記述方法を演習します。

- 使用するTaskパラメータの指定
  - 使用するTaskパラメータの指定は、前節で設定したtaskRefと同じ箇所に`params`項目を追記することで指定します。具体的には、`params`配下に、`name`項目を記述することで指定します。この演習として、~/ci-practice/handson/pipelines/handson-pipeline.yamlをviエディタで開き、以下に示す`<REPLACE-WITH-PARAM-NAME>`を先程確認したソースコード取得(git-clone)Taskのパラメータ名(=url)に変更して下さい。

```
  tasks:
  - name: git-clone-health
    taskRef:
      name: <REPLACE-WITH-TASK-NAME>
    params:
    - name: <REPLACE-WITH-PARAM-NAME>
      value: $(params.git-url)
```      

- 使用するTaskパラメータへの値設定確認
  - 使用するTaskパラメータへの値設定は、`tasks.params`配下に`value`項目を記述することで指定します。本演習で使用する~/ci-practice/handson/pipelines/handson-pipeline.yamlでは、設定済みの記述`$(params.git-url)`を使用しますので、編集作業は、不要となります。
  - `$(params.git-url)`は、以下に示す箇所において、このPipeline自体に宣言されたパラメータであることを確認します。このパラメータは、PipelineRunから値を設定することで活用されます。

```
apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: handson-pipeline
...<省略>
  params:  
  - name: git-url
    type: string
```

- ご参考: 各リソース間のパラメータ設定の関係性に関しては、以下スライドをご参照下さい。

![picture 7](./handson/img/images/655a7f9713e4d9c3354904b64773073e1635b3450a10fce0a899afbe5c764f13.png)  


#### 2.3.3 Pipeline作成
Pipelineの作成は、編集した~/ci-practice/handson/pipelines/handson-pipeline.yamlを使用して、以下コマンドによりPipelineリソースを作成します。

```
cat ~/ci-practice/handson/pipelines/handson-pipeline.yaml | envsubst | oc apply -f - -n ci-${USER}
```

- 補足: `envsubst`は、handson-pipeline.yamlで使用されている環境変数を実際の値に展開するコマンドです。本演習では、ユーザーアカウント毎にSonarQubeサーバー等の資材を使用するため、ユーザーアカウント毎の設定を環境変数で表現しており、その環境変数設定を反映するための対応となります。

<details>
  <summary>正解用のコマンド</summary>

```
cat ~/ci-practice/handson/answers/handson-pipeline.yaml | envsubst | oc apply -f - -n ci-${USER}
```
</details>

以下コマンドにより、Pipelineが作成されていることを確認します。
```
oc get pipeline -n ci-${USER}
```

出力例
```
NAME               AGE
handson-pipeline   7s
```

以上をもちまして、Pipelineの作成は、完了となります。

### 2.4 `PipelineRun`の定義作成
本節では、下図赤枠で示されるPipelineRunリソースを定義を作成します。具体的には、PipelineRunリソース定義作成として、前節で作成したPipelineをPipelineRunより参照する形で定義し、また、Pipelineで宣言したパラメータに値を設定する記述確認を行います。

![picture 8](./handson/img/images/83ccb4ac485fd01af5b42faeff4d4c19d320d2dbe1b25a4042ccdc0cf236165c.png)  


#### 2.4.1 Pipeline参照の定義

`PipelineRun`からのPipeline参照は、`pipelineRef`を設定することで行います。~/ci-practice/handson/pipelines/handson-pipelinerun.yamlを開き、以下`<REPLACE_WITH_PIPELINE_NAME>`を前節で定義したPipelineの名前で置換して下さい。

```
apiVersion: tekton.dev/v1beta1
kind: PipelineRun
metadata:
  generateName: handson-pipeline-run-
spec:
  pipelineRef:
    name: <REPLACE_WITH_PIPELINE_NAME>
```

**ヒント**
Pipelineの名前は、`metadata.name`で定義された値となります。

```
apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: <この値が、Pipelineの名前>
```

#### 2.4.2 パラメータ値の設定
本節では、PipelineRunから参照するPipelineのパラメータに対して、値を設定する方法を確認します(=編集作業は、不要です)。具体的には、前節で作成したPipelineで宣言されているパラメータgit-urlに対して、値設定を行う記述を確認します。

本演習では、個々人のユーザーアカウント毎に異なるGitレポジトリを使用する便宜上、~/ci-practice/handson/pipelines/handson-pipelinerun.yamlにおいて、以下の様な環境変数GIT_APP_URLを使用した値設定を行います。具体的には、PipelineRun作成時に、envsubstコマンドにより環境変数に格納された値に置換することで、パラメータgit-urlに対して値設定を行います。

```
  params:
    <省略>
    - name: git-url
      value: '${GIT_APP_URL}'
```      

### 2.5 環境設定と動作確認

#### 2.5.1 git-clone用のSecret作成
本演習で使用するGitサーバーであるGiteaにアクセスするためには、ユーザー名/パスワード情報を使用する必要があります。作成済みのソースコード取得(git-clone)Taskが、Giteaにアクセスするためには、OpenShiftリソースであるSecretを作成し、そのSecret内にGiteaにアクセスするためのユーザー名/パスワード情報を格納する必要があります。[参考: Tektonドキュメント](https://tekton.dev/vault/pipelines-v0.14.3/auth/#basic-authentication-git)

最初に、~/ci-practice/handson/pipelines/git-auth.yamlを開き、内容を確認して下さい。

次に、以下コマンドを実行し、Secretの作成を行います。

```
export GIT_URL=http://$(oc -n ${USER}-develop get route gitea -o jsonpath='{.spec.host}')
cat ~/ci-practice/handson/pipelines/git-auth.yaml | envsubst | oc apply -f - -n ci-${USER}
oc secrets link pipeline git-creds -n ci-${USER}
```

以下、上記コマンドの説明となります。
- oc secrets link <ServiceAccount名> <Secret名>
  - 指定ServiceAccount(例: pipeline)から、指定Secret(例: git-creds)を使用可能とする設定

以上をもちまして、git-clone用のSecret作成は、完了となります。

#### 2.5.2 Workspace用PersistentVolumeClaimの作成
Workspaceは、TaskやStep間でファイル等の資材を共有するために使用する、共有ディスクとしての機能を提供します。例えば、本演習で作成するCIパイプラインの場合、ソースコード取得(git-clone)Taskで取得したソースコードをWorkspaceに格納し、次に実行する静的診断(sonarqube-scanner)Taskにて、同Workspaceのソースコードにアクセスし、静的診断を行う際に、Workspaceが、使用されます。

Tektonでは、Workspaceとして、PersistentVolumeやConfigMap等、幾つかのVolumeSourceを指定可能ですが、本演習では、PersistentVolumeを使用してWorkspace作成を行います。
[参考: Tekton VolumeSources](https://tekton.dev/docs/pipelines/workspaces/#specifying-volumesources-in-workspaces)

最初に、~/ci-practice/handson/pipelines/handson-pipelinerun.yamlを開き、以下の様な、`persistentVolumeClaim`記述を確認します。これにより、PersistentVolumeを使用するWorkspace設定であることを確認します。

```
spec:
  pipelineRef:
    name: <REPLACE_WITH_PIPELINE_NAME>
  ...<省略>
  workspaces:
    - name: shared-workspace
      persistentVolumeClaim:
        claimName: shared-workspace
```        

次に、~/ci-practice/handson/pipelines/tekton-pvc.yamlを開き、PersistentVolumeClaim名(=shared-workspace)が、先に確認したWorkspace設定で使用される`claimName`と同じであることを確認します。

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: shared-workspace
  ...<以下略>
```

次に、以下コマンドにより、PersistentVolumeClaimを作成します。

```
oc apply -f ~/ci-practice/handson/pipelines/tekton-pvc.yaml -n ci-${USER}
```

以上で、Workspace用PersistentVolumeClaimの作成は、完了となります。

#### 2.5.3 CIパイプライン動作確認
以下コマンドにより、`PipelineRun` を作成し、パイプラインが実行されていることを確認します。
```
export GIT_APP_URL=http://$(oc -n ${USER}-develop get route gitea -o jsonpath='{.spec.host}')/gitea/cicd-sample-app
cat ~/ci-practice/handson/pipelines/handson-pipelinerun.yaml | envsubst | oc create -f - -n ci-${USER}
```

<details>
  <summary>正解用のコマンド</summary>

```
cat ~/ci-practice/handson/answers/handson-pipeline.yaml | envsubst | oc apply -f - -n ci-${USER}
```
</details>

以下コマンドにより、PipelineRunが作成されていることを確認します。
```
oc get pipelinerun -n ci-${USER}
```

出力例
```
NAME                         SUCCEEDED   REASON    STARTTIME   COMPLETIONTIME
handson-pipeline-run-sjt7n   Unknown     Running   5s          
```

OpenShiftコンソールの画面左側メニューの`パイプライン`からパイプライン実行を選択し、実行状況を確認します。

- 画面左側メニュー「パイプライン」 
![picture 11](./handson/img/images/a500c6e92d8567042c4e95c3a55bdddea67ce1a94404b0d001d08ddf4bdcd11a.png)  

- CIパイプライン実行ステータス確認画面
![tips](./handson/img/pipeline-success.png)

- CIパイプライン実行ログ確認画面
![picture 12](./handson/img/images/2642588b9b58d55a9b4d253948e4a14a4bcd0ed3be8548fa8e8a5b98f7c577af.png)  


CIパイプライン実行完了まで少々時間がかかりますので、少々お待ち頂いた上で、CIパイプライン実行ステータスが成功になるかご確認下さい。もし、`PipelineRun` が失敗する場合、ログやイベントから原因を確認し、適宜マニフェストファイルを修正した後、あらためて実行して下さい。また、原因究明のために、ご遠慮なく、担当講師にご相談下さい。

## 3. Event Listenerの作成
本章では、以下図の赤枠で示すEvent Listener作成方法の学習を目的として、外部Gitレポジトリ(本演習では、Gitea)からのwebhookを契機として前章で作成したCIパイプラインを実行するためのEvent Listenerの作成を行います。

![picture 16](./handson/img/images/70693bd309c5c13da438652285c8b6f0ad11409a6955b255cdc41b43d967954b.png)  

具体的には、以下図で示すTekton Trigger定義リソースである`TriggerBinding`、`TriggerTemplate`、及び、`EventListener`の作成を行います。

![picture 17](./handson/img/images/5b80a78b87ef9facfae7fa18e7265b34805218557ea6286ef6b6f90516420db6.png)  

### 3.1 `TriggerBinding`の作成
TriggerBindingは、外部Gitサーバーのwebhook通知を受信し、その通知データをTriggerTemplateへパラメータ経由で引き渡す設定を行うためのリソースです。本演習では、~/ci-practice/handson/triggers/handson-binding.yamlを使用して、TriggerBindingを作成します。

~/ci-practice/handson/triggers/handson-binding.yamlを開き、webhook通知データが、どのようなパラメータに値として設定されているか、確認して下さい。(例: webhook通知データのrepository.clone_urlは、どのパラメータに値として設定されているか?)

次に、以下コマンドにより、TriggerBindingを作成します。
```
cat ~/ci-practice/handson/triggers/handson-binding.yaml | oc apply -f - -n ci-${USER}
```
以下コマンドにより、TriggerBindingが作成されていることを確認します。

```
oc get triggerbinding -n ci-${USER}
```

出力例
```
NAME              AGE
handson-binding   12s
```

### 3.2 `TriggerTemplate`の作成
TriggerTemplateは、TriggerBindingより渡されるパラメータ値を使用して、PipelineRunを作成するリソースです。本演習では、このパラメータ値設定の記述を理解することを目的とした演習を行います。

最初に、~/ci-practice/handson/triggers/handson-template.yamlを開き、以下の様に、`<REPLACE_WITH_PARAM_NAME>`と記述されている箇所を確認します。そして、`<REPLACE_WITH_PARAM_NAME>`に何が設定されるべきかを考えて、適切な値で置き換えて下さい。

```
apiVersion: triggers.tekton.dev/v1beta1
kind: TriggerTemplate
metadata:
  name: handson-template
spec:
  params:
    - name: git-app-rev
      description: The git revision
      default: main
    - name: <REPLACE_WITH_PARAM_NAME>
      description: The git repository url
```      

**ヒント**
- `<REPLACE_WITH_PARAM_NAME>`は、パラメータ宣言であり、git cloneに使用するURLを格納するパラメータです。
- このパラメータに値を設定するリソースは、前節で作成したTriggerBindingですので、TriggerBindingで値を設定する際に使用したパラメータ名は、`<REPLACE_WITH_PARAM_NAME>`と同じ文字列となります。
- 別のヒントとして、`<REPLACE_WITH_PARAM_NAME>`によるパラメータ宣言は、TriggerTemplateのPipelineRun作成時に使用されるパラメータです。PipelineRun作成時は、`$(tt.params.<REPLACE_WITH_PARAM_NAME>)`の形式でパラメータ値が参照されますので、~/ci-practice/handson/triggers/handson-template.yamlの以下PipelineRun生成記述に正解となる文字列が含まれています。

```
  resourcetemplates:
    - apiVersion: tekton.dev/v1beta1
      kind: PipelineRun
      metadata:
        generateName: handson-run-
      spec:
        pipelineRef:
          name: handson-pipeline
        params:
          - name: target-path
            value: 'site'
          - name: git-url
            value: $(tt.params.git-app-url)
          - name: git-revision
            value: $(tt.params.git-app-rev)
```            
- 上記ヒントに関して、下図もご参照下さい。

![picture 18](./handson/img/images/cf291f0989e273e81d916660a0adba50147d3d312a0709d19f2e712f7201075a.png)  


次に、`<REPLACE_WITH_PARAM_NAME>`設定後、以下コマンドにより、TriggerTemplateを作成します。
```
oc apply -f ~/ci-practice/handson/triggers/handson-template.yaml -n ci-${USER}
```

<details>
  <summary>正解用のコマンド</summary>

```
oc apply -f ~/ci-practice/handson/answers/handson-template.yaml -n ci-${USER}
```
</details>

以下コマンドにより、TriggerTemplateの作成を確認します。
```
oc get triggertemplate -n ci-${USER}
```

出力例
```
NAME               AGE
handson-template   23s
```

### 3.3 `EventListener`の作成
EventListenerは、使用するTriggerBindingとTriggerTemplateを指定、及びInterceptor設定を記述するために使用します。本節では、これらの設定を行い、理解を深めることを目的とします。

#### 3.3.1 `TriggerBinding`と`TriggerTemplate`の参照設定
`TriggerBinding`と`TriggerTemplate`の参照設定は、EventListener内にて、`ref`記述をすることで定義します。~/ci-practice/handson/triggers/handson-listener.yamlを開き、以下の様に、`<REPLACE_WITH_TriggerBinding_NAME>`、及び、`<REPLACE_WITH_TriggerTemplate_NAME>`と記述されている箇所を確認して下さい。また、どのような文字列が設定されるべきかを考え、適宜編集置換して下さい。

```
apiVersion: triggers.tekton.dev/v1beta1
kind: EventListener
metadata:
  name: handson-listener
spec:
  triggers:
    - bindings:
        - ref: <REPLACE_WITH_TriggerBinding_NAME>
      template:
        ref: <REPLACE_WITH_TriggerTemplate_NAME>
```

#### 3.3.2 `Interceptor`の設定
同じく、~/ci-practice/handson/triggers/handson-listener.yamlを開き、以下の様なInterceptor設定内容を確認します。

```
      interceptors:
      - ref:
          name: "github"
          kind: ClusterInterceptor
        params:
        - name: "secretRef"
          value:
            secretName: git-webhook
            secretKey: secretkey
        - name: "eventTypes"
          value: ["push"]
```

以下、説明となります。
- github
  - [GitHub Interceptor](https://tekton.dev/vault/triggers-main/interceptors/#github-interceptors)と呼ばれる、GitHubのwebhookに特化したInterceptor使用の指定
- secretName/secretKey
  - Gitサーバーからのwebhookの認証の際に使用するSecret名とそのkey-valueデータのkey値
- entryTypes
  - Gitサーバーからのwebhookのタイプが、pushである時のみ、後続のTriggerBindingへ処理を進めます。

次に、上記Secretによる認証機能をInterceptorで使用するために、Secretの作成を行います。以下コマンドを実行して下さい。また、作成したSecretは、ServiceAccountであるpipelineより使用出来るよう、oc secrets linkコマンドを実行して下さい。

```
export SECRET_TOKEN=$(cat /dev/urandom | tr -dc '[:alpha:]' | fold -w ${1:-20} | head -n 1)
oc create secret generic git-webhook --from-literal=secretkey=${SECRET_TOKEN} -n ci-${USER}
oc secrets link pipeline git-webhook -n ci-${USER}
```

#### 3.3.3 `EventListener`の作成
最後に、上記で編集した~/ci-practice/handson/triggers/handson-listener.yamlより、EventListenerを以下コマンドにより作成します。

```
oc apply -f ~/ci-practice/handson/triggers/handson-listener.yaml -n ci-${USER}
```

<details>
  <summary>正解用のコマンド</summary>

```
oc apply -f ~/ci-practice/handson/answers/handson-listener.yaml -n ci-${USER}
```
</details>

以下コマンドにより、EventListenerの作成を確認します。
```
oc get eventlistener -n ci-${USER}
```

出力例
```
NAME               ADDRESS                                                 AVAILABLE   REASON                     READY   REASON
handson-listener   http://el-handson-listener.xxx.svc.cluster.local:8080   True        MinimumReplicasAvailable   True   
```

### 3.4 Route作成
Route作成は、Gitサーバーからのwebhook通知を受信するために、行います。以下コマンドを実行してください。

```
oc expose service el-handson-listener -n ci-${USER} 
```

以下コマンドにより、Routeの作成を確認します。
```
oc get route -n ci-${USER} 
```
出力例
```
NAME                  HOST/PORT                                   PATH   SERVICES              PORT            TERMINATION   WILDCARD
el-handson-listener   el-handson-listener-xxx.openshiftapps.com          el-handson-listener   http-listener                 None
```

### 3.5 動作確認
本章での動作確認は、curlコマンドを使用して、Event Listenerの起動、及びInterceptorの動作確認を行います。具体的には、curlコマンドにより、secret未設定でHTTPリクエストを送信し、Interceptorにより、HTTPリクエストが拒否されることを確認します(正しいsecretを使用した動作確認は、次章で行います)。

```
curl -H 'X-GitHub-Event: push' -X POST -H 'Content-Type: application/json' -d '{"after": "xxx", "repository": {"clone_url": "yyy"}}' http://$(oc -n ci-${USER} get route el-handson-listener -o jsonpath='{.spec.host}')
```

以下コマンドにより、Event Listener Podのログを確認し、`"interceptor stopped trigger processing: rpc error: code = FailedPrecondition desc = no X-Hub-Signature header set"`のメッセージから、HTTPリクエストが拒否されたことを確認します。
```
oc logs $(oc -n ci-${USER} get pod --no-headers | grep el-handson-listener | awk '{print $1}') -n ci-${USER} 
```

以上をもちまして、Event Listener作成は、完了となります。

## 4. GitリポジトリへのソースコードpushからCIパイプライン実行までの動作確認
本章では、作成したCIパイプラインの活用方法を体験して頂く目的で、Gitリポジトリへのソースコードを契機にCIパイプライン実行を行うための設定作業、及び、その動作確認を行います。

### 4.1. GitリポジトリへのWebhook設定
Webhookを有効にするため、GiteaのWeb Consoleから`EventListener`に対し Webhookを設定します。最初に、WEBブラウザからアプリケーションのGitリポジトリを開きます。リポジトリのURLは、以下コマンドで確認します。
```
echo "$(oc -n ${USER}-develop get route gitea -o jsonpath='{.spec.host}')/gitea/cicd-sample-app"
```

次に、ブラウザ上で開いたGitリポジトリの画面右上の`サインイン`からログインし、ユーザ名に `gitea`、パスワードに `openshift` を入力します。
> 上手く画面が表示されない場合は、画面左上の`Dashboard` => `Repositories配下のgitea/cicd-sample-app`を実行してみて下さい。

次に、画面右上の **Settings**(下図右の赤枠)をクリックします。 

![picture 19](./handson/img/images/ac1a85100a09f281a56322a3a7d4021e9d8d0ddd0e20c8d6a8e2276381eb00fd.png)  

次の画面にて、**Webhooks** -> **Add Webhook** -> **Gitea**を選択します。

![picture 20](./handson/img/images/44de04a627c25d9210e5e48634d89573d39889ff54d4b36b7550a39554d0d2ea.png)  


次に Webhookに関する以下の項目を設定します。

* Target URL: 前章で作成した`EventListener`のURLを指定します。具体的には、
`echo http://$(oc -n ci-${USER} get route el-handson-listener -o jsonpath='{.spec.host}')`コマンドで出力されるURLを設定します。

* Secret: 前章で作成した`SECRET_TOKEN`の値を指定します。具体的には、
`oc -n ci-${USER} get secret git-webhook -o go-template='{{.data.secretkey | base64decode}}'`コマンドで出力される値を設定します。

* Trigger On: `Push Events`にチェックを入れます。

* Branch filterに`master`を指定します。

設定画面例
![picture 29](./handson/img/images/a1da888be0e4884d35a29d9f44af5aaab30955923bc2cfe79f5cc6122c32dba1.png)  


次に、`Add Webhook`ボタンを押し、設定情報を保存します。次に、作成した Webhookの設定をクリックします。

![picture 21](./handson/img/images/d1c42238aaa1daf910ce72fc749c33ace0381d99860d64268c8d7630a1995c82.png)  

次に、画面右下の`Test Delivery` をクリックします。この`Test Delivery`により、テスト用のwebhook通知が、指定Target URLへ送信され、CIパイプライン実行が、開始されます。

![picture 22](./handson/img/images/4c3b796a52e3597c4dae0fd8d65bd79f2234927c94acd43818381802df77faf0.png)  


前章と同様の手順で、OpenShiftコンソールから新たなパイプライン実行が開始されていることを確認して下さい。

### 4.2. 実際の開発プロセスを想定したCIパイプライン動作確認
ここまで、CIパイプラインを実行するために必要な設定を適用しましたが、最後に実際の開発プロセスにおいて CIパイプラインがどのように実行されるか確認します。

前提条件として、アプリケーションの Gitリポジトリは、[GitHub Flow](https://docs.github.com/en/get-started/quickstart/github-flow)をベースに開発が行われ、`main` と `feature` 2つのブランチが存在しています。また、開発者は `feature`ブランチで新機能を開発し、変更を`main` ブランチにマージすると想定します。  

![githubflow](./handson/img/githubflow.png)

最初に、WebブラウザからアプリケーションのGitリポジトリを開き、`feature`ブランチを作成します。Webブラウザ上から、**Code** -> **1 Branch**をクリックし、**Create new branch from 'master'** ボタンをクリックします。

![picture 23](./handson/img/images/6eda2ddfe0ba03d4662709b22fc6b033f12f655c15e784dc728e1da0fd778d35.png)  

次に、表示されるダイアログ画面のBranch Name に`feature`と入力し、**Create branch** をクリックします。

![picture 24](./handson/img/images/0a0fbb9336de79aad1636044f5d3e956feeef4b20df70fd439bb500a736677dc.png)  

次に、ソースコードの開発編集を想定して、`/site/public/index.html`ファイルを選択し、下図赤枠の`Edit File`をクリックして編集画面を開きます。

![picture 25](./handson/img/images/0f5ba4e826f977278e2ee2f25a5c99ec1b96a02c15abb16e48344b3c71f7ec98.png)  


74と77行目付近のコメントアウトを削除し、<div>要素を有効化します。
```
# before
      <!--
      <div class="box">
        <div class="map" id='map'></div>
      </div>
      -->

# after
      <div class="box">
        <div class="map" id='map'></div>
      </div>
```
その後、画面下の**Commit Changes** をクリックし、変更をコミットします。Commit Changes ボックス内のメッセージは任意で構いません。

![commit](./handson/img/commit.png)  

画面上の **Pull Requests** -> **New Pull Request** から新しいプルリクエストの作成を開始します。以下の様に、`merge into: gitea:master`、及び、`pull from: gitea:feature`を選択します。

![picture 26](./handson/img/images/e1ad13681e6929514cc420162252e602a46fd43ae463a1133b5a855f914b430a.png)  


次に、**New Pull Request** -> **Create Pull Request**ボタンをクリックし、プルリクエストを作成します。
![pr](./handson/img/pullrequest.png)

次に 作成したプルリクエストを**Pull Requests**より選択して開きます。

![picture 27](./handson/img/images/d4316499bc5d100d67ef1ba0b9e60b56bf4ae6530894542f368aab20f82caef4.png)  


画面中部に表示される**Merge Pull Request** ボタンをクリックし、もう1度、**Merge Pull Request** ボタンをクリックします。これにより、`feature` ブランチに追加した変更が `master`ブランチに取り込まれるとともに、Webhookにより、CIパイプラインが実行されます。

最後に OpenShiftコンソールからパイプラインが実行されることを確認します。 
**Pipelines** -> **PipelineRuns** から新しくパイプラインが実行され、ステータスが `成功`であれば成功です。(実行完了まで、コンテナイメージビルド等に少々時間がかかりますので、ご了承下さい)

![picture 28](./handson/img/images/fadb92181372d19c2abf0e79c6ba5c66d3b48b4c5ce7af0badfb8fa5dd9ea60a.png)  

以上で本ハンズオンは終了です。お疲れ様でした。



