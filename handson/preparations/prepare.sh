#!/bin/bash

USERNUM=$1
HANDSON_NAMESPACE="handson"
CD_REPO=https://gitlab.com/openshift-starter-kit/cd-practice/
CD_REPO_NAME=$(basename ${CD_REPO})

ID=$(oc whoami)
if [ ${ID} == system:admin -o ${ID} == cluster-admin ]; then
  true
else
  echo "Login as cluster-admin or system:admin user with the oc login command."
fi

if test $# != 1;then
 echo "prepare.sh <handson-usernum>"
 exit
fi

# OpenShift Pipelines Operator
echo "$(date '+%F %T') Installing OpenShift Pipelines Operator" 
oc apply -f files/openshift-pipelines-operator.yaml
sleep 30

while ! oc get csv -n openshift-operators -l operators.coreos.com/openshift-pipelines-operator-rh.openshift-operators | grep Succeeded
do
  sleep 20
done

# OpenShift Gitops Operator" 
echo "$(date '+%F %T') Installing OpenShift Gitops Operator" 
oc apply -f files/openshift-gitops-operator.yaml
sleep 30

while ! oc get csv -n openshift-gitops-operator -l operators.coreos.com/openshift-gitops-operator.openshift-gitops-operator | grep Succeeded
do
  sleep 20
done

# Web Terminal Operator."
echo "$(date '+%F %T') Installing Web Terminal Operator."
oc apply -f files/web-terminal-operator-subs.yaml
sleep 30

while ! oc get csv -n openshift-operators -l operators.coreos.com/web-terminal.openshift-operators | grep Succeeded
do
  sleep 20
done

while ! oc get csv -n openshift-operators -l operators.coreos.com/devworkspace-operator.openshift-operators | grep Succeeded
do
  sleep 20
done

# OpenShift Logging Operator."
echo "$(date '+%F %T') Installing OpenShift Logging Operator."
oc apply -f files/openshift-logging-operator.yaml
sleep 10

while ! oc get csv -n openshift-logging -l operators.coreos.com/cluster-logging.openshift-logging | grep Succeeded
do
  sleep 10
done

create_htpasswd() {
  secret=$(oc get oauth cluster -ojsonpath='{.spec.identityProviders[].htpasswd.fileData.name}')
  # Extract the opentlc-mgr user credentials with the cluster-admin privilege.
  oc get -n openshift-config secret ${secret} -o go-template='{{.data.htpasswd | base64decode}}' | egrep '(opentlc-mgr|admin)' > users.htpasswd
  for userid in $(seq 1 ${USERNUM}); do
    # For the whole hands-on.
    htpasswd -b users.htpasswd user${userid} openshift
    oc create user user${userid}
    # For the RBAC hands-on.
    htpasswd -b users.htpasswd user${userid}-dev openshift
    oc create user user${userid}-dev
    # For the monitoring hands-on.
    htpasswd -b users.htpasswd user${userid}-ops openshift
    oc create user user${userid}-ops
  done
}

echo "$(date '+%F %T') Creating hands-on users with the HTPasswd identity provider."
TYPE=$(oc get oauth cluster -ojsonpath='{.spec.identityProviders[].type}')
if [ ${TYPE} = "HTPasswd" ] ; then
# If you deploy the cluster using OpenShift 4.10 Workshop on RHPDS, you won't have to create users i.e. user1, user2, etc since it aready enables HTPasswd IdP and you can specify the number of users when provisionig the cluster.
# However, update all the passwords for the users anyway here and add other users in case it's not specified.
  create_htpasswd
  oc -n openshift-config set data secret ${secret} --from-file=htpasswd=users.htpasswd
else
  # If the HTpasswd IdP is not set, set the HTpasswd IdP and create users.
  oc apply -f files/oauth.yaml
  create_htpasswd
  oc -n openshift-config create secret generic handson-htpass-secret --from-file=htpasswd=users.htpasswd
fi

for userid in $(seq 1 ${USERNUM}); do
  users+=("user${userid}")
done

# Add the hands-on users to the group with the cluste-admin role.
oc adm groups new handson-cluster-admins "${users[@]}"
oc adm policy add-cluster-role-to-group cluster-admin handson-cluster-admins --rolebinding-name=handson-cluster-admins

echo "$(date '+%F %T') Deploying Etherpad, PostgreSQL, and Apache."
oc new-project ${HANDSON_NAMESPACE}
oc process postgresql-persistent --param-file=files/etherpad-db-template-param.txt --labels=app=etherpad_db -n openshift | oc apply -f -
oc process -f https://raw.githubusercontent.com/wkulhanek/docker-openshift-etherpad/master/etherpad-template.yaml --param-file=files/etherpad-template-param.txt | oc apply -f -
oc new-app --name=httpd httpd~./text
oc expose svc httpd

echo "$(date '+%F %T') Deploying OpenShift Logging." 
oc apply -f files/clusterlogging.yaml

echo "$(date '+%F %T') Extending timeout and replacing the image with gettext (envsubs) for Web Terminal Operator."
oc -n openshift-operators annotate devworkspacetemplate -l console.openshift.io/terminal=true web-terminal.redhat.com/unmanaged-state="true"
oc -n openshift-operators patch devworkspacetemplate web-terminal-exec --type merge --type='json' -p '[{"op": "replace", "path": "/spec/components/0/container/env/0/value", "value":"6h"}]'
oc -n openshift-operators patch devworkspacetemplate web-terminal-tooling -n openshift-operators --type merge --type='json' -p '[{"op": "replace", "path": "/spec/components/0/container/image", "value":"quay.io/kshiraka/custom-web-terminal-tooling-rhel8:1.5-starterkit"}]'

echo "$(date '+%F %T') Enableing user-defined monitoring for projects for Ops monitoring Hands-on."
oc apply -f files/clustermonitoring-configmap.yaml
# Create the hands-on project in advance to give the group the permission to create the monitoring resources.
for userid in $(seq 1 ${USERNUM}); do
  oc new-project user${userid}-ops
  oc -n user${userid}-ops adm policy add-role-to-user monitoring-edit user${userid}-ops
  oc -n user${userid}-ops adm policy add-role-to-user admin user${userid}-ops
done

echo "$(date '+%F %T') Deploying Gitea, SonarCube."
cd ../../demo/
for userid in $(seq 1 ${USERNUM}); do
    oc login -u user${userid} -p openshift $(oc whoami --show-server)
    ./demo.sh install -p user${userid}
    ./demo.sh install -p user${userid} --conf-repo ${CD_REPO} --conf-repo-name ${CD_REPO_NAME}
done

oc login -u system:admin

# Allow hands-on users to modify ArgoCD custom resources. Cf. https://gitlab.com/openshift-starter-kit/cd-manifest/-/commit/c6e81cc58a9463572e49716a66667ea777126b14
# Otherwise, ArgoCD can't connect to the git repository.
echo "$(date '+%F %T') Setting RBAC for CD Hands-on."
oc create clusterrolebinding appprojects-edit --clusterrole=appprojects.argoproj.io-v1alpha1-edit --group=system:authenticated
oc create clusterrolebinding applications-edit --clusterrole=applications.argoproj.io-v1alpha1-edit --group=system:authenticated
oc patch argocd openshift-gitops -n openshift-gitops --type='json' -p='[{"op": "replace", "path": "/spec/rbac/policy", "value":"g, system:authenticated, role:admin\n"}]'
for userid in $(seq 1 ${USERNUM}); do
  oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n user${userid}-develop
  oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n user${userid}-staging
  oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n user${userid}-production
done
