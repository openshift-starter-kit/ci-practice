#!/bin/bash

export USER=$(oc whoami)

# Cloning the manifest repository
git clone http://$(oc -n ${USER}-develop get route gitea -o jsonpath='{.spec.host}')/gitea/ci-practice

# Cloning the application repository
git clone http://$(oc -n ${USER}-develop get route gitea -o jsonpath='{.spec.host}')/gitea/cicd-sample-app

oc new-project ci-${USER}

# Task
oc apply -f ~/ci-practice/handson/tasks/git-clone.yaml -n ci-${USER}
oc apply -f ~/ci-practice/handson/tasks/sonarqube.yaml -n ci-${USER}
oc apply -f ~/ci-practice/handson/tasks/trivy.yaml -n ci-${USER}
oc get task -n ci-${USER}

# Pipeline
envsubst < ~/ci-practice/handson/answers/handson-pipeline.yaml | oc apply -f - -n ci-${USER}
oc get pipeline -n ci-${USER}

# Git Repo Secret
export GIT_URL=http://$(oc -n ${USER}-develop get route gitea -o jsonpath='{.spec.host}')
cat ~/ci-practice/handson/pipelines/git-auth.yaml | envsubst | oc apply -f - -n ci-${USER}
oc secrets link pipeline git-creds -n ci-${USER}

# PVC
oc apply -f ~/ci-practice/handson/pipelines/tekton-pvc.yaml -n ci-${USER}

# PipelineRun
export GIT_APP_URL=http://$(oc -n ${USER}-develop get route gitea -o jsonpath='{.spec.host}')/gitea/cicd-sample-app
envsubst < ~/ci-practice/handson/answers/handson-pipelinerun.yaml | oc create -f - -n ci-${USER}
oc get pipelinerun -n ci-${USER}

# EventListener
oc apply -f ~/ci-practice/handson/triggers/handson-binding.yaml -n ci-${USER}
oc apply -f ~/ci-practice/handson/answers/handson-template.yaml -n ci-${USER}
export SECRET_TOKEN=$(cat /dev/urandom | tr -dc '[:alpha:]' | fold -w ${1:-20} | head -n 1)
oc create secret generic git-webhook --from-literal=secretkey=${SECRET_TOKEN} -n ci-${USER}
oc secrets link pipeline git-webhook -n ci-${USER}
oc apply -f ~/ci-practice/handson/answers/handson-listener.yaml -n ci-${USER}
oc expose service el-handson-listener -n ci-${USER} 
