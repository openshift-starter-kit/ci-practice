# 概要

OCP クラスタ構築後のハンズオンに必要な設定を適用します。
- Identity Provider (HTPasswd) の設定とハンズオン用ユーザの登録
- Etherpad (+ PostgreSQL) のデプロイ: ユーザからの質問や進捗管理などのメモ用
- Apache のデプロイ: コンテンツのシェア (任意)
- Web Terminal Operator のデプロイ: ユーザのオペレーション用

次に CICD ハンズオン環境をセットアップします。
- OpenShift Pipelines Operator (Tekton) のデプロイ: ハンズオンに用いる Git リポジトリを GitLab (gitlab.com) からクローンするため
- Git リポジトリ (Gitea) のデプロイ: ハンズオンに用いる Git リポジトリを格納
- SonarQube のデプロイ: パイプラインの静的解析


# 前提

OpenShift クラスタを構築した後、スクリプトを実行するホストに以下のコマンドがインストール済みであることが前提となります。

- oc (OpenShift CLI)
- htpasswd (httpd-tools)

# 実行方法

- 環境の準備

```
$ git clone https://gitlab.com/openshift-starter-kit/ci-practice
$ cd ci-practice/handson/preparations/
$ oc login -u cluster-admin -p <PASSWORD> <OpenShift_API_URL>
$ ./prepare.sh <handson-usernum>
```
`./prepare.sh 10` のように `<handson-usernum>` にハンズオンのユーザ数を指定します。

- CI ハンズオンの事前検証

このスクリプトは [3. Event Listenerの作成](https://gitlab.com/openshift-starter-kit/ci-practice/-/tree/main#3-event-listener%E3%81%AE%E4%BD%9C%E6%88%90) までを実行します。
[4. GitリポジトリへのソースコードpushからCIパイプライン実行までの動作確認](https://gitlab.com/openshift-starter-kit/ci-practice/-/tree/main#4-git%E3%83%AA%E3%83%9D%E3%82%B8%E3%83%88%E3%83%AA%E3%81%B8%E3%81%AE%E3%82%BD%E3%83%BC%E3%82%B9%E3%82%B3%E3%83%BC%E3%83%89push%E3%81%8B%E3%82%89ci%E3%83%91%E3%82%A4%E3%83%97%E3%83%A9%E3%82%A4%E3%83%B3%E5%AE%9F%E8%A1%8C%E3%81%BE%E3%81%A7%E3%81%AE%E5%8B%95%E4%BD%9C%E7%A2%BA%E8%AA%8D) は実行しないため、手動で検証します。
```
$ oc login -u <USER> -p <PASSWORD> <OpenShift_API_URL>
$ ./validate.sh
```
`oc login -u user1` のように `<USER>` にハンズオンのユーザ名を指定します。

- Etherpad および説明・ハンズオン資料をホストする httpd の URL 確認

```
$ oc -n handson status
```
または
```
$ oc -n handson get route
```

- 説明・ハンズオン資料の更新

資料を `text/` 配下に格納後にスクリプトを実行します。
```
update-text.sh
```
